//
//  ViewController.swift
//  appClima
//
//  Created by Laboratorio FIS on 25/10/17.
//  Copyright © 2017 Laboratorio FIS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK:- Objetos
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginImage: UIImageView!
    @IBOutlet weak var userText: UITextField!
    @IBOutlet weak var passText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    //Cuando no edito objetos se cierra el teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    //MARK:- Actions
    @IBAction func aceptarButtonLogin(_ sender: Any) {
        let user=userText.text ?? ""
        let password=passText.text ?? ""
        switch(user,password){
        case ("alexis","alexis"):
            print("Login Correcto")
            performSegue(withIdentifier: "cuidadSegue", sender: self)
        case("alexis",_)://Cualquier cosa _
            print("Contraseña Incorrecta")
        default:
            print("Usuario y contraseña incorrecta")
        }
    }
    
    
    @IBAction func enlaceButtonLogin(_ sender: Any) {
    }
}

